package com.example.android.pacpacmonsters.classes;

import android.graphics.Bitmap;

import java.util.ArrayList;

/*
* Abstract class from which all LFs will
* inherit
* SPeggs
*  List of most common pets:
*  fish (goldfish?), dog, bird, cat, ferret,  iquanas, rabbits, turtle, snakes, horses
*  guinea pig, hedgehog, rat, sugar glider, gerbil, hamster, mouse, sheep, chinchilla, gecko, goat
 */
public abstract class LifeForm {
    private LFID id = LFID.NONE;
    private int level = 1;
    private int experiencePoints = 1;
    private String name = "";
    private String nickName = "nickname";
    private String description = "";
    private int HPTotal = 1;
    private int HPCurrent = 1;
    private int Attack = 1;
    private int Defense = 1;
    private int Speed = 1;
    private int Special = 1;
    //Kingdom, phylum, class, order, family, genus, species

    //this needs to be lowered to capture a Lifeform
    private int sedation = 0;

    private int Accuracy = 1;
    //Kilograms
    private double weight = 10.0;
    //Meters
    private double height = 1.0;
    private Bitmap thumbnail;
    private Bitmap front;//Battling against
    private Bitmap back;//Battling with

    public enum LFtype{
        WATER,
        FIRE,
        AIR,
        EARTH,
        ELECTRIC,
        ICE,
        PLANT,
        VENOM,
        NONE
    }
    private LFtype type = LFtype.NONE;

    public enum LFcondition{
        NONE,
        DROWNING,
        BURNED,
        APOXIA,
        SHOOK,
        SHOCKED,
        FROSTBITE,
        ENTANGLED,
        POISONED
    }
    private LFcondition condition = LFcondition.NONE;

    public enum LFID{
        EARTH_DOG,
        FIRE_CAT,
        AIR_BIRD,
        WATER_FISH,
        NONE
    }

    private Character.Gender gender = Character.Gender.NONE;

    private ArrayList<Move> moves;

    //Each LF implements it's own leveling and evolving in this function
    public abstract void addExperiencePoints(int XP);

    //abstract void attack(int i);
    //abstract void flee();//This one might not be abstract in the end
    //abstract void ability();


    //Getters and Setters

    public ArrayList<Move> getMoves(){
        return moves;
    }

    public void addMove(Move move){
        moves.add(move);
    }

    public void removeMove(int i){
        if(moves.size() > i){
            moves.remove(i);
        }
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int i){
        if(this.level < i){
            if(i > 100){
                this.level = 100;
            }
            else{
                this.level = i;
            }
        }
    }

    public int getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(int xp){
        this.experiencePoints = xp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getHealthPointsTotal() {
        return HPTotal;
    }

    public void setHPTotal(int HPTotal) {
        this.HPTotal = HPTotal;
    }

    public int getHPCurrent() {
        return HPCurrent;
    }

    public void setHPCurrent(int HPCurrent) {
        this.HPCurrent = HPCurrent;
    }

    public void damage(int damage){
        if(HPCurrent > damage){
            HPCurrent -= damage;
        }else{
            HPCurrent = 0;
            sedation = 10;
        }
        sedation += 10*damage/HPTotal;
        if(sedation > 10){
            sedation = 10;
        }
    }

    public Character.Gender getGender() {
        return gender;
    }

    public void setGender(Character.Gender gender) {
        this.gender = gender;
    }

    public LFtype getType() {
        return type;
    }

    public void setType(LFtype type) {
        this.type = type;
    }

    public LFcondition getCondition() {
        return condition;
    }

    public void setCondition(LFcondition condition) {
        this.condition = condition;
    }

    public int getHPTotal() {
        return HPTotal;
    }

    public int getAttack() {
        return Attack;
    }

    public void setAttack(int attack) {
        Attack = attack;
    }

    public int getDefense() {
        return Defense;
    }

    public void setDefense(int defense) {
        Defense = defense;
    }

    public int getSpeed() {
        return Speed;
    }

    public void setSpeed(int speed) {
        Speed = speed;
    }

    public int getSpecial() {
        return Special;
    }

    public void setSpecial(int special) {
        Special = special;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Bitmap getFront() {
        return front;
    }

    public void setFront(Bitmap front) {
        this.front = front;
    }

    public Bitmap getBack() {
        return back;
    }

    public void setBack(Bitmap back) {
        this.back = back;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LFID getId() {
        return id;
    }

    public void setId(LFID id) {
        this.id = id;
    }


    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public int getSedation() {
        return sedation;
    }

    public void setSedation(int sedation) {
        this.sedation = sedation;
    }

    public static int log(int x, int base){
        return (int) (Math.log(x) / Math.log(base));
    }
}
