package com.example.android.pacpacmonsters.classes.LifeForms;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Moves.Bite;
import com.example.android.pacpacmonsters.classes.Moves.Soak;

//See puffer fish for reference, they have controllable skin color like a octopus
public class WaterFish extends LifeForm {
    public void WaterFish(int level){
        setId(LFID.WATER_FISH);
        setName("Water Fish");
        setLevel(level);
        setType(LifeForm.LFtype.WATER);
        setDescription("A beaked fish, uses a jet for short bursts of speed, " +
                "puffs up when threatened, really likes crabs.");
        setHeight(0.3);
        setWeight(20);

        //need to randomize? and balance this
        setAttack(level*3);
        setDefense(level*3);
        setSpeed(level*3);
        setSpecial(level*5);
        setHPTotal(level*6);
        setHPCurrent(level*6);
        setAccuracy(level*5);

        addMove(new Bite());
        addMove(new Soak());
    }
    public void addExperiencePoints(int XP){
        setExperiencePoints(getExperiencePoints()+XP);
        //10 xp = level 1, 100 xp = level 2, 1000 xp = level 3 etc
        if(getLevel() < log(getExperiencePoints(), 2)){
            setLevel(log(getExperiencePoints(), 2));
        }
    }
}
