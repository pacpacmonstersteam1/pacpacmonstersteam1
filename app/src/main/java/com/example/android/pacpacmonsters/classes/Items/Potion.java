package com.example.android.pacpacmonsters.classes.Items;

import com.example.android.pacpacmonsters.classes.Character;
import com.example.android.pacpacmonsters.classes.Item;
import com.example.android.pacpacmonsters.classes.LifeForm;

public class Potion extends Item {
    public void Potion(){
        setName("Potion");
        setDescription("Restore 50 HP points to a Life Form.");
    }

    @Override
    public void use(Character user){
        //todo toast
    }

    @Override
    public void useOnLF(Character user, LifeForm lifeForm){
        if(lifeForm.getHealthPointsTotal() -  lifeForm.getHPCurrent() < 50){
            lifeForm.setHPCurrent(lifeForm.getHPTotal());
        }else{
            lifeForm.setHPCurrent(lifeForm.getHPCurrent() + 50);
        }
    }
}
