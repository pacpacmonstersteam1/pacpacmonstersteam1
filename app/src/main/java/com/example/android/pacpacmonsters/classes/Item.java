package com.example.android.pacpacmonsters.classes;

/*
* Abstract class from which all items will inherit
* SPeggs
 */
public abstract class Item {
    private String name = "";
    private String description = "";

    public abstract void use(Character user);
    public abstract void useOnLF(Character user, LifeForm lifeForm);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
