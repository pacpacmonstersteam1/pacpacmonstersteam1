package com.example.android.pacpacmonsters.classes.Moves;


import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Move;

public class Stone extends Move {
    public void Stone(){
        setType(LifeForm.LFtype.EARTH);
        setName("Stone");
        setDescription("Strike target with a stone.");
    }

    //water > fire > air > earth  > water ...
    @Override
    public void doMove(LifeForm self, LifeForm target) {
        int damage = self.getAttack() + self.getSpecial()*2 + 10;
        if(target.getType() == LifeForm.LFtype.WATER){
            damage*=2;
        }else if (target.getType() == LifeForm.LFtype.AIR){
            damage = damage / 2;
        }
        target.damage(damage - target.getDefense());
    }
}
