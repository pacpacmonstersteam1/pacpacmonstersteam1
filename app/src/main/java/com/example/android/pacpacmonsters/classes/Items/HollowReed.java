package com.example.android.pacpacmonsters.classes.Items;

import com.example.android.pacpacmonsters.classes.Character;
import com.example.android.pacpacmonsters.classes.Item;
import com.example.android.pacpacmonsters.classes.LifeForm;

import java.util.Calendar;

public class HollowReed extends Item {
    public void HollowReed(){
        setName("Hollow Reed");
        setDescription("Can be used to shoot light projectiles such as darts or to" +
                "swim underwater.");
    }

    @Override
    public void use(Character user){
        //todo toast this is a non-usable item, it should say 'nows not the time for that'
    }

    @Override
    public void useOnLF(Character user, LifeForm lifeForm){
    }
}
