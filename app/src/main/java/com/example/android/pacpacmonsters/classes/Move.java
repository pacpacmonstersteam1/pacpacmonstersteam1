package com.example.android.pacpacmonsters.classes;

public abstract class Move{
    private String name = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description = "";
    private LifeForm.LFtype type = LifeForm.LFtype.WATER;
    private int baseDamage = 1;

    public abstract void doMove(LifeForm self, LifeForm target);//This is where the special code will be added

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LifeForm.LFtype getType() {
        return type;
    }

    public void setType(LifeForm.LFtype type) {
        this.type = type;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }
}
