package com.example.android.pacpacmonsters.classes.LifeForms;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Moves.Bite;
import com.example.android.pacpacmonsters.classes.Moves.Burn;

public class FireCat extends LifeForm {
    public void FireCat(int level){
        setId(LFID.FIRE_CAT);
        setName("Fire Cat");
        setLevel(level);
        setType(LFtype.FIRE);
        setDescription("A cat with fiery eyes and sharp claws, not for everyone.");
        setHeight(0.5);
        setWeight(15);

        //need to randomize this
        setAttack(level*4);
        setDefense(level*3);
        setSpeed(level*4);
        setSpecial(level*7);
        setHPTotal(level*5);
        setHPCurrent(level*5);
        setAccuracy(level*10);

        addMove(new Bite());
        addMove(new Burn());
    }
    public void addExperiencePoints(int XP){
        setExperiencePoints(getExperiencePoints()+XP);
        //10 xp = level 1, 100 xp = level 2, 1000 xp = level 3 etc
        if(getLevel() < log(getExperiencePoints(), 2)){
            setLevel(log(getExperiencePoints(), 2));
        }
    }
}
