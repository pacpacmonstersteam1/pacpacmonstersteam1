package com.example.android.pacpacmonsters.classes;

import android.location.Location;
import android.support.annotation.ColorInt;

import java.util.ArrayList;
import java.util.Date;

public class Character {
    private static String email = "";
    private static int level = 1;
    private static int expieriencePoints = 1;
    private static int money = 1;
    private static int temperature = 70;
    private static int windSpeed = 5;
    //humidity pressure
    private static Location lastKnownLocation;
    private static String Username = "New User";

    private static Date smartLureTimer = new Date(100, 1, 1);
    private static Date Birthday = new Date(100, 1, 1); //2000, 1, 1
    //0xff0000ff opaque blue - 0xff00ff00 opaque green - 0xffff0000 opaque red
    private static int A = 16, R = 8, G = 8, B = 8;
    @ColorInt
    private static int skin = (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff);
    @ColorInt
    private static int eyes = 0xff888888;//opaque grey?
    @ColorInt
    private static int hair = 0xff888888;
    @ColorInt
    private static int shirt = 0xff888888;
    @ColorInt
    private static int pants = 0xff888888;

    private static ArrayList<Item> bag;
    private static ArrayList<Item> box;
    private static ArrayList<LifeForm> pack;
    private static ArrayList<LifeForm> menagerie;
    private static ArrayList<LifeForm.LFID> DocumentedLFs = new ArrayList<>();

    public enum Gender{
        FEMALE,
        MALE,
        NONE
    }

    public enum Precipitation{
        NONE,
        RAIN,
        SNOW
    }

    private static Gender gender = Gender.NONE;

    public Character(){
        bag = new ArrayList<Item>();
        box = new ArrayList<Item>();
        pack = new ArrayList<LifeForm>();
        menagerie = new ArrayList<LifeForm>();
    }

    public static ArrayList<LifeForm> getPack(){
        return pack;
    }

    public static void addLFPack(LifeForm lifeForm){
        if(!DocumentedLFs.contains(lifeForm.getId())){
            DocumentedLFs.add(lifeForm.getId());
        }
        if(pack.size() < 8) {
            pack.add(lifeForm);
        }else{
            menagerie.add(lifeForm);
        }
    }

    public static void removeLFPack(int i){
        if(pack.size() > i){
            pack.remove(i);
        }
    }

    public static ArrayList<LifeForm> getMenagerie(){
        return menagerie;
    }

    public static void addLFMenagerie(LifeForm lifeForm){
        menagerie.add(lifeForm);
    }

    public static void removeLFMenagerie(int i){
        if(menagerie.size() > i){
            menagerie.remove(i);
        }
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        Character.email = email;
    }

    public static int getLevel() {
        return level;
    }

    public static void setLevel(int level) {
        Character.level = level;
    }

    public static int getExpieriencePoints() {
        return expieriencePoints;
    }

    public static void setExpieriencePoints(int expieriencePoints) {
        Character.expieriencePoints = expieriencePoints;
        if(level < log(Character.expieriencePoints, 2)){
            setLevel(log(Character.expieriencePoints, 2));
        }
    }

    public static String getUsername() {
        return Username;
    }

    public static void setUsername(String username) {
        Username = username;
    }

    public static Date getBirthday() {
        return Birthday;
    }

    public static void setBirthday(Date birthday) {
        Birthday = birthday;
    }

    public static Gender getGender() {
        return gender;
    }

    public static void setGender(Gender gender) {
        Character.gender = gender;
    }

    public static int getSkin() {
        return skin;
    }

    public static void setSkin(int skin) {
        Character.skin = skin;
    }

    public static int getEyes() {
        return eyes;
    }

    public static void setEyes(int eyes) {
        Character.eyes = eyes;
    }

    public static int getHair() {
        return hair;
    }

    public static void setHair(int hair) {
        Character.hair = hair;
    }

    public static int getShirt() {
        return shirt;
    }

    public static void setShirt(int shirt) {
        Character.shirt = shirt;
    }

    public static int getPants() {
        return pants;
    }

    public static void setPants(int pants) {
        Character.pants = pants;
    }

    public static int getMoney() {
        return money;
    }

    public static void setMoney(int money) {
        Character.money = money;
    }

    public static Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public static void setLastKnownLocation(Location lastKnownLocation) {
        Character.lastKnownLocation = lastKnownLocation;
    }

    public static Date getSmartLureTimer() {
        return smartLureTimer;
    }

    public static void setSmartLureTimer(Date smartLureTimer) {
        Character.smartLureTimer = smartLureTimer;
    }


    public static int log(int x, int base){
        return (int) (Math.log(x) / Math.log(base));
    }
}
