package com.example.android.pacpacmonsters.classes.Moves;


import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Move;

public class Bite extends Move{

    public void Bite(){
        setType(LifeForm.LFtype.NONE);
        setName("Bite");
        setDescription("Bite the target.");
    }

    @Override
    public void doMove(LifeForm self, LifeForm target){
        target.damage(self.getAttack()*2 - target.getDefense() + 5);
    }
}

