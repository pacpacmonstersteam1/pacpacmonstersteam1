package com.example.android.pacpacmonsters.classes.Moves;


import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Move;

public class Squall extends Move {

    public void Squall(){
        setType(LifeForm.LFtype.AIR);
        setName("Squall");
        setDescription("Buffet the target with a blast of wind and debris.");
    }

    //water > fire > air > earth  > water ...
    @Override
    public void doMove(LifeForm self, LifeForm target) {
        int damage = self.getAttack() + self.getSpecial()*2 + 10;
        if(target.getType() == LifeForm.LFtype.EARTH){
            damage*=2;
        }else if (target.getType() == LifeForm.LFtype.FIRE){
            damage = damage / 2;
        }
        target.damage(damage - target.getDefense());
    }
}
