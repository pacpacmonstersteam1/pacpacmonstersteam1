package com.example.android.pacpacmonsters.classes.Moves;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Move;


public class Soak extends Move {

    public void Soak(){
        setType(LifeForm.LFtype.WATER);
        setName("Soak");
        setDescription("Hits target with a surge of water, usually aimed at the mouth and nose.");
    }

    //water > fire > air > earth  > water ...
    @Override
    public void doMove(LifeForm self, LifeForm target) {
        int damage = self.getAttack() + self.getSpecial()*2 + 10;
        if(target.getType() == LifeForm.LFtype.FIRE){
            damage*=2;
        }else if (target.getType() == LifeForm.LFtype.EARTH){
            damage = damage / 2;
        }
        target.damage(damage - target.getDefense());
    }
}
