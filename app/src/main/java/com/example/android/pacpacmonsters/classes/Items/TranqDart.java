package com.example.android.pacpacmonsters.classes.Items;

import com.example.android.pacpacmonsters.classes.Character;
import com.example.android.pacpacmonsters.classes.Item;
import com.example.android.pacpacmonsters.classes.LifeForm;

public class TranqDart extends Item{
    public void TranqDart(){
        setName("Tranq Dart");
        setDescription("Reduces a Life Form's Wokeness, making it easier to domesticate.");
    }

    @Override
    public void use(Character user){
        //todo toast
    }

    @Override
    public void useOnLF(Character user, LifeForm lifeForm){
        lifeForm.setSedation(lifeForm.getSedation() + 1);
    }
}
