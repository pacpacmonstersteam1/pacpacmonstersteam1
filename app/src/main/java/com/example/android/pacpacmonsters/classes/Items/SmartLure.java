package com.example.android.pacpacmonsters.classes.Items;

import com.example.android.pacpacmonsters.classes.Character;
import com.example.android.pacpacmonsters.classes.Item;
import com.example.android.pacpacmonsters.classes.LifeForm;

import java.util.Calendar;

public class SmartLure extends Item {
    public void SmartLure(){
        setName("Smart Lure");
        setDescription("Uses a variety of sounds and smells to attract fauna.");
    }

    @Override
    public void use(Character user){
        user.setSmartLureTimer(Calendar.getInstance().getTime());
    }

    //TODO move the character into a global application override class for easy access
    @Override
    public void useOnLF(Character user, LifeForm lifeForm){
        user.setSmartLureTimer(Calendar.getInstance().getTime());
    }
}
