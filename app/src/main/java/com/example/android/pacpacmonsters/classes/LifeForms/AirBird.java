package com.example.android.pacpacmonsters.classes.LifeForms;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Moves.Bite;
import com.example.android.pacpacmonsters.classes.Moves.Burn;
import com.example.android.pacpacmonsters.classes.Moves.Squall;

//See great horned owl for reference
public class AirBird extends LifeForm{
    public void AirBird(int level){
        setId(LFID.AIR_BIRD);
        setName("Air Bird");
        setLevel(level);
        setType(LFtype.AIR);
        setDescription("This owl stores pressurized air inside its hollow bones during the day, when " +
                "it is time for fight or flight it blasts the air out of specialized wing feathers.");
        setHeight(0.7);
        setWeight(10);

        //need to randomize this
        setAttack(level*6);
        setDefense(level*2);
        setSpeed(level*8);
        setSpecial(level*5);
        setHPTotal(level*4);
        setHPCurrent(level*4);
        setAccuracy(level*10);

        addMove(new Bite());
        addMove(new Squall());
    }

    public void addExperiencePoints(int XP){
        setExperiencePoints(getExperiencePoints()+XP);
        //10 xp = level 1, 100 xp = level 2, 1000 xp = level 3 etc
        if(getLevel() < log(getExperiencePoints(), 2)){
            setLevel(log(getExperiencePoints(), 2));
        }
    }
}
