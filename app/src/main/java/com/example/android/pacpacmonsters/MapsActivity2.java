package com.example.android.pacpacmonsters;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import android.content.res.Resources;

public class MapsActivity2 extends FragmentActivity implements
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = MapsActivity2.class.getSimpleName();


    private GoogleMap mMap;
    private Location mLastKnownLocation;

    private Marker mAir;
    private Marker mEarth;
    private Marker mFire;
    private Marker mWater;


    //NYC
    private LatLng mDefaultLocation = new LatLng(40.7128, -74.006);

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    private Marker positionMarker;
    //private Circle accuracyCircle;

    private TextView coordsTV;

    private boolean mLocationPermissionGranted = false;
    //Todo user option to enable/disable this
    private boolean mRequestingLocationUpdates = false;

    private int LOCATION_REQUEST_CODE = 123;
    private int REQUEST_CHECK_SETTINGS = 124;
    private String REQUESTING_LOCATION_UPDATES_KEY = "REQUESTING_LOCATION_UPDATES_KEY";

    //2.0 - 21.0 higher is a closer zoom level
    private float DEFAULT_ZOOM = 19.0f;
    private int INTERVAL = 2000;
    private int MIN_INTERVAL = 1000;

    private int updateCount = 0;
    private int startersCollected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        updateValuesFromBundle(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new 	StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

         coordsTV = findViewById(R.id.coordsTextView);

        createLocationRequest();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    // Todo figure out which location we want to use
                    // ...
                }
                //toast(s);


                mLastKnownLocation = locationResult.getLastLocation();
                String s = "";
                s += mLastKnownLocation.getLatitude() + ", " + mLastKnownLocation.getLongitude() +
                        "\nBearing: " + mLastKnownLocation.getBearing() + " deg Speed: " + mLastKnownLocation.getSpeed() + " m/s" +
                        "\nUpdate Count: " + updateCount;
                coordsTV.setText(s);
                double latitude = mLastKnownLocation.getLatitude();
                double longitude = mLastKnownLocation.getLongitude();

                if (positionMarker != null) {
                    positionMarker.remove();
                }

                //BitmapDescriptor character = BitmapDescriptorFactory.fromResource(R.drawable.chardown);
//                BitmapDescriptor character = BitmapDescriptorFactory.fromBitmap(characterBitmap);

                //Bitmap characterBitmap = resizeMapIcons("chardown", 100,100);
                //Bitmap charBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.chardown);
                //Bitmap resizedCharBitmap = Bitmap.createScaledBitmap(charBitmap, 100, 100, false);

                //BitmapDescriptor character = BitmapDescriptorFactory.fromBitmap(charBitmap);

                double bearing = mLastKnownLocation.getBearing();
                BitmapDescriptor character;


                if(bearing >= 315 || bearing <= 45){//North
                    character = BitmapDescriptorFactory.fromResource(R.drawable.charupbig);
                }else if(bearing > 45 && bearing < 135){//East
                    character = BitmapDescriptorFactory.fromResource(R.drawable.charrightbig);
                }else if(bearing >= 135 && bearing <= 225){//South
                    character = BitmapDescriptorFactory.fromResource(R.drawable.chardownbig);
                }else{//West
                    character = BitmapDescriptorFactory.fromResource(R.drawable.charleftbig);
                }


                final MarkerOptions positionMarkerOptions = new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .icon(character)
                        .anchor(0.5f, 0.5f);
                positionMarker = mMap.addMarker(positionMarkerOptions);

                //mMap.moveCamera(CameraUpdateFactory.newLatLng(
                //        new LatLng(mLastKnownLocation.getLatitude(),
                 //               mLastKnownLocation.getLongitude())));

                updateCount++;
                if(updateCount == 3){
                    addStarters();
                }
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(mLastKnownLocation.getLatitude(),
//                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
            };
        };

        //Should prompt the user, called in the launch activity too
        checkPermission();


        //Location
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        //Once the async call to get the map returns from Google API execution continues in onMapReady()
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    //Coming from OnCreate Async call
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //There no way to get here without permission, still the result is handled in onRequestPermissionResult()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            //mMap.setMyLocationEnabled(true);
            //mMap.getUiSettings().setMyLocationButtonEnabled(true);

        } else {
            toast("Location Permission Denied.");
            checkPermission();
        }

        //mMap.setOnMyLocationButtonClickListener(this);
        //mMap.setOnMyLocationClickListener(this);
        mMap.setOnMarkerClickListener(this);

        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        //mMap.getUiSettings().setScrollGesturesEnabled(false);
        //mMap.getUiSettings().setZoomGesturesEnabled(false);
        mMap.setBuildingsEnabled(false);


        //mMap.getUiSettings().setScrollGesturesEnabled(true);
        //mMap.getUiSettings().setZoomGesturesEnabled(true);

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json_aubergine));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }


        //Todo continueWithTask instead of nested callbacks?
        //Now that we have our map we can dispatch our second Async call to get the device location
        getDeviceLocation();
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    //Coming from onMapReady Callback
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = (Location) task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            //todo Check Current Location Settings
                            mRequestingLocationUpdates = true;
                            startLocationUpdates();

                            //todo Start listening for user location here
                        } else {
                            //Log.d(TAG, "Current location is null. Using defaults.");
                            //Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /*
    * add starters for the player to select
    * Latitude ranges from -90 to 90, longitude ranges from -180 to 180
    * so we add twice the longitude for the same displacement
     */
    private void addStarters(){
        double latitude = mLastKnownLocation.getLatitude();
        double longitude = mLastKnownLocation.getLongitude();

        BitmapDescriptor airicon = BitmapDescriptorFactory.fromResource(R.drawable.typeair);
        final MarkerOptions airMarkerOptions = new MarkerOptions()
                .position(new LatLng(latitude + 0.0001, longitude))
                .title("Earth Type Life Form")
                .snippet("Earth Type Life Form")
                .icon(airicon).anchor(0.5f, 0.5f)
                .infoWindowAnchor(0.5f, 0.5f);
        mAir = mMap.addMarker(airMarkerOptions);

        BitmapDescriptor earthicon = BitmapDescriptorFactory.fromResource(R.drawable.typeearth);
        final MarkerOptions earthMarkerOptions = new MarkerOptions()
                .position(new LatLng(latitude - 0.0001, longitude))
                .title("Air Type Life Form")
                .snippet("Air Type Life Form")
                .icon(earthicon).anchor(0.5f, 0.5f)
                .infoWindowAnchor(0.5f, 0.5f);
        mEarth = mMap.addMarker(earthMarkerOptions);

        BitmapDescriptor fireicon = BitmapDescriptorFactory.fromResource(R.drawable.typefire);
        final MarkerOptions fireMarkerOptions = new MarkerOptions()
                .position(new LatLng(latitude, longitude + 0.0002))
                .title("Fire Type Life Form")
                .snippet("Fire Type Life Form")
                .icon(fireicon).anchor(0.5f, 0.5f)
                .infoWindowAnchor(0.5f, 0.5f);
        mFire = mMap.addMarker(fireMarkerOptions);

        BitmapDescriptor watericon = BitmapDescriptorFactory.fromResource(R.drawable.typewater);
        final MarkerOptions waterMarkerOptions = new MarkerOptions()
                .position(new LatLng(latitude, longitude - 0.0002))
                .title("Water Type Life Form")
                .snippet("Water Type Life Form")
                .icon(watericon).anchor(0.5f, 0.5f)
                .infoWindowAnchor(0.5f, 0.5f);
        mWater = mMap.addMarker(waterMarkerOptions);

        //positionMarker = mMap.addMarker(positionMarkerOptions);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        double latitude = mLastKnownLocation.getLatitude();
        double longitude = mLastKnownLocation.getLongitude();

        if(marker.equals(mAir)){
            toast("Air");
        }else if(marker.equals(mEarth)){
            toast("Earth");
        }else if(marker.equals(mFire)){
            toast("Fire");
        }else if(marker.equals(mWater)){
            toast("Water");

        }

            marker.setVisible(false);
            startersCollected++;
            if(startersCollected == 1){
                mAir.setPosition(new LatLng(latitude + 0.001, longitude));
                mEarth.setPosition(new LatLng(latitude - 0.001, longitude));
                mFire.setPosition(new LatLng(latitude , longitude + 0.002));
                mWater.setPosition(new LatLng(latitude, longitude - 0.002));
            }else if(startersCollected == 2){
                mAir.setPosition(new LatLng(latitude + 0.01, longitude));
                mEarth.setPosition(new LatLng(latitude - 0.01, longitude));
                mFire.setPosition(new LatLng(latitude , longitude + 0.02));
                mWater.setPosition(new LatLng(latitude, longitude - 0.02));
            }else if(startersCollected == 3){
                mAir.setPosition(new LatLng(latitude + 0.1, longitude));
                mEarth.setPosition(new LatLng(latitude - 0.1, longitude));
                mFire.setPosition(new LatLng(latitude , longitude + 0.2));
                mWater.setPosition(new LatLng(latitude, longitude - 0.2));
            }else if(startersCollected == 4){
                toast("Win");
                startersCollected = 0;
                mAir.setPosition(new LatLng(latitude + 0.0001, longitude));
                mEarth.setPosition(new LatLng(latitude - 0.0001, longitude));
                mFire.setPosition(new LatLng(latitude , longitude + 0.0002));
                mWater.setPosition(new LatLng(latitude, longitude - 0.0002));
                mAir.setVisible(true);
                mEarth.setVisible(true);
                mFire.setVisible(true);
                mWater.setVisible(true);

            }
        return true;
    }

    private void checkLocationSetting(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());



        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MapsActivity2.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        try {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback,
                    null /* Looper */);
        }catch(SecurityException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
                mRequestingLocationUpdates);
        // ...
        super.onSaveInstanceState(outState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }

        // Update the value of mRequestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
            mRequestingLocationUpdates = savedInstanceState.getBoolean(
                    REQUESTING_LOCATION_UPDATES_KEY);
        }

        // ...

        // Update UI to match restored state
        //updateUI();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(MIN_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    //We could use this listener but it is redundant given mLocationListener
    @Override
    public void onLocationChanged(Location location) {

    }

    //From implements Location Listener
    @Override
    public void onProviderEnabled(String provider){

    }

    @Override
    public void onProviderDisabled(String provider){

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle b){

    }

    //Check if we have the needed location permissions for getting the location, should prompt user if need be
    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ){//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true;
                try {
                    //mMap.setMyLocationEnabled(true);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            } else {
                toast("Location Permission Denied.");
            }
        }
    }

    public Bitmap resizeMapIcons(String iconName,int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    private void toast(String s){
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }
}
