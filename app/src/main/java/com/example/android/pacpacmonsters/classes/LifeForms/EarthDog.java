package com.example.android.pacpacmonsters.classes.LifeForms;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Moves.Bite;
import com.example.android.pacpacmonsters.classes.Moves.Stone;


public class EarthDog extends LifeForm{
    public void EarthDog(int level){
        setId(LFID.EARTH_DOG);
        setName("Earth Dog");
        setLevel(level);
        setType(LifeForm.LFtype.EARTH);
        setDescription("Eats dirt and rocks, very friendly, its paws are smooth river stones.");
        setHeight(0.5);
        setWeight(20);

        //need to randomize this
        setAttack(level*4);
        setDefense(level*3);
        setSpeed(level*3);
        setSpecial(level*4);
        setHPTotal(level*7);
        setHPCurrent(level*7);
        setAccuracy(level*3);

        addMove(new Bite());
        addMove(new Stone());
    }
    public void addExperiencePoints(int XP){
        setExperiencePoints(getExperiencePoints()+XP);
        //10 xp = level 1, 100 xp = level 2, 1000 xp = level 3 etc
        if(getLevel() < log(getExperiencePoints(), 2)){
            setLevel(log(getExperiencePoints(), 2));
        }
    }
}
