package com.example.android.pacpacmonsters.classes.Items;

import com.example.android.pacpacmonsters.classes.Character;
import com.example.android.pacpacmonsters.classes.Item;
import com.example.android.pacpacmonsters.classes.LifeForm;

public class Sack extends Item {
    public void Sack(){
        setName("Sack");
        setDescription("Use on a Life Form with high sedation (>7) to domesticate.");
    }

    @Override
    public void use(Character user){
        //TODO toast
    }

    @Override
    public void useOnLF(Character user, LifeForm lifeForm){
        if(lifeForm.getSedation() > 7 && lifeForm.getWeight() < 50 && lifeForm.getHeight() < 3){
            user.addLFPack(lifeForm);
        }else{
            //todo toast
        }
    }
}
