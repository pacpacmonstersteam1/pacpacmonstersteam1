package com.example.android.pacpacmonsters.classes.Moves;

import com.example.android.pacpacmonsters.classes.LifeForm;
import com.example.android.pacpacmonsters.classes.Move;

//water > fire > air > earth  > water ...
public class Burn extends Move {
     public Burn(){
          setType(LifeForm.LFtype.FIRE);
          setName("Burn");
          setDescription("Scorches the target with super heated air.");
     }

     @Override
     public void doMove(LifeForm self, LifeForm target) {
          int damage = self.getAttack() + self.getSpecial()*2 + 5;
          if(target.getType() == LifeForm.LFtype.AIR){
               damage*=2;
          }else if (target.getType() == LifeForm.LFtype.WATER){
               damage = damage / 2;
          }
          target.damage(damage - target.getDefense());
     }
}
